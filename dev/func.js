export async function RunScript(s1, b, meta = false) {
    const {eElement} = meta ? await import("./h/mob.js") : await import("./h/disk.js");
    let headElement = await eElement()
    for (let element of headElement) {
        document.head.append(element);
    }
    if (meta) {
        document.querySelector("html").classList.value = "media-mobile js-focus-visible"
        document.body.classList.value = 'en  '
    } else {
        document.querySelector("html").classList.value = "media-desktop js-focus-visible"
        document.body.classList.value = 'en  login-or-register-page'
    }
    let Nodes = document.createRange().createContextualFragment(b);
    for (let element of Nodes.childNodes) {
        document.body.append(element);
    }
    const script = document.createElement('script');
    script.src = s1;
    script.async = true;
    script.type = "text/javascript";
    document.head.append(script);
    script.onload = function bolder(e) {
        let checkExist = setInterval(function () {
            console.log(document.readyState)
            if (document.readyState === 'complete') {
                document.body.style.opacity = '1';
                clearInterval(checkExist);
            }
        }, 100);
    }
    script.onerror = function (event, source, lineno, colno, error) {
        console.log({event, source, lineno, colno, error})
    }
}
